import { createApp } from 'vue'
import App from './App.vue'
import './js/module/geolocation.module'

createApp(App).mount('#app')
