

const updateLocation = (position) => {

    const item = `
        широта: ${position.coords.latitude},
        <br/>
        долгота: ${position.coords.longitude},
        <br/>
        точность(м): ${position.coords.accuracy},
        <br/>
        высота: ${position.coords.altitude},
        <br/>
        точность высоты: ${position.coords.altitudeAccuracy},
        <br/>
        heading: ${position.coords.heading},
        <br/>
        скорость движения: ${position.coords.speed},
    `
    const p = document.createElement('p')
    const span = document.createElement('span')
    document.body.appendChild(p)
    document.body.appendChild(span)
    document.querySelector('p').innerHTML = item



}


const updateStatus = (msg) => {
    document.body.innerHTML = msg;
}

const handleError = (err) => {
    switch (err.code) {
        case 0:
            updateStatus('При попытке оределить возникла ошибка' + err.message)
            break;
        case 1:
            updateStatus('Пользователь запретил получение данных о местоположении')
            break;
        case 2:
            updateStatus('Браузеру не удалось определить местоположение' + err.message)
            break;
        case 3:
            updateStatus('Истекло доступное время ожидания')
            break;
    }
}

window.addEventListener('load', () => {
    if ("geolocation" in navigator) {

        document.body.innerHTML = 'Ваш браузер поддерживает Geolocation API !'
        navigator.geolocation.getCurrentPosition(updateLocation, handleError, {
            enableHighAccuracy: true,
        })

    } else {

        document.body.innerHTML = 'Ваш браузер НЕ поддерживает Geolocation API !'

    }
})



